clc;
clear all;
close all;


AVERAGE_OVER_EXPERIMENTS = 1 % Switch
IDS_TO_USE = [1,2,3,4]
EXPORT_TO_PDFS = 1
EXPORT_TO_PNGS = 0

database = 'benchmark_modified.db';
db = mksqlite('open', database);
tables = mksqlite('show tables')

planner_names = mksqlite('select * from known_planner_configs'); % Width
for i=1:(size(tables,1))  % Find the number of runs
    name = tables(i).tablename;
    if strcmp(name,'known_planner_configs') || strcmp(name,'experiments')|| strcmp(name,'sqlite_sequence')
        continue;
    else
        table = mksqlite(['select * from ' name]);
        break;
    end
end
number_of_runs = size(table,1); % Height

% Data holding structures
path_plan_length=zeros(number_of_runs,size(planner_names,1)); %MUST BE ZEROS
solved = double(zeros(1,size(planner_names,1)));
path_plan_time = NaN(number_of_runs,size(planner_names,1));
path_plan_smoothness = NaN(number_of_runs,size(planner_names,1));
path_simplify_length = NaN(number_of_runs,size(planner_names,1));
path_simplify_smoothness = NaN(number_of_runs,size(planner_names,1));

for i=1:(size(tables,1)) % Data acquisition
    name = tables(i).tablename;
    if strcmp(name,'known_planner_configs') || strcmp(name,'experiments')|| strcmp(name,'sqlite_sequence')
        continue;
    else
        table = mksqlite(['select * from ' name]);
        for j=1:number_of_runs
            if ~AVERAGE_OVER_EXPERIMENTS && ~any(IDS_TO_USE==table(j).experimentid)
                continue;
            end
            
            % Solved
            if table(j).solved== 1
                solved(1,table(1).plannerid)=solved(1,table(1).plannerid)+1;
            else
                continue
            end
            % Path plan length
            if isField(table,'path_plan_length')
                if isempty(table(j).path_plan_length) || (table(j).path_plan_length) == 0
                    path_plan_length(j,table(1).plannerid)=NaN;
                else
                    path_plan_length(j,table(1).plannerid)= table(j).path_plan_length;
                end
            end
            
            
            % Path plan time
            if isField(table,'path_plan_time')
                if isempty(table(j).path_plan_time) || (table(j).path_plan_time) == 0
                    path_plan_time(j,table(1).plannerid)=NaN;
                else
                    path_plan_time(j,table(1).plannerid)= table(j).path_plan_time;
                end
            end
            % Path plan smoothness
            if isField(table,'path_plan_smoothness')
                if isempty(table(j).path_plan_smoothness)
                    path_plan_smoothness(j,table(1).plannerid)=NaN;
                else
                    path_plan_smoothness(j,table(1).plannerid)= table(j).path_plan_smoothness;
                end
            end
            % Path simplify length
            if isField(table,'path_simplify_length')
                if isempty(table(j).path_simplify_length)|| (table(j).path_simplify_length)== 0
                    path_simplify_length(j,table(1).plannerid)=NaN;
                else
                    path_simplify_length(j,table(1).plannerid)= table(j).path_simplify_length;
                end
            end
            % Path simplify smoothness
            if isField(table,'path_simplify_smoothness')
                if isempty(table(j).path_simplify_smoothness)|| (table(j).path_simplify_smoothness) == 0
                    path_simplify_smoothness(j,table(1).plannerid)=NaN;
                else
                    path_simplify_smoothness(j,table(1).plannerid)= table(j).path_simplify_smoothness;
                end
            end
        end
    end
end

path_plan_length(all(path_plan_length==0,2),:)=[]; % Remove redundant rows
% path_plan_time(all(path_plan_time==0,2),:)=[];
% path_plan_smoothness(all(path_plan_smoothness==0,2),:)=[];
% path_simplify_length(all(path_plan_smoothness==0,2),:)=[];
% path_simplify_smoothness(all(path_plan_smoothness==0,2),:)=[];
number_of_runs = size(path_plan_length,1)
path_plan_length(path_plan_length==0) = NaN;

axis = struct2cell(planner_names); % Create labels for the graphs
axis(1,:)=[];
axis(2,:)=[];
axis = strrep(axis, 'OMPL_', '')
axis = strrep(axis, 'kConfigDefault', '  ')
axis = strrep(axis, 'nnect', 'n.')

graf1 = figure;
boxplot(path_plan_length,'Labels',axis,'plotstyle','compact')
title 'Path plan length'
txt = findobj(gca,'Type','text');
set(txt,'FontSize', 11,'VerticalAlignment','middle','Rotation',89);
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]); 
grid on
if(EXPORT_TO_PDFS)
    print(graf1,'-dpdf', 'path_plan_length.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf1,'-dpng', 'path_plan_length.png');
end

graf3 = figure;
boxplot(path_plan_time,'Labels',axis,'plotstyle','compact')
title 'Path planning time'
txt = findobj(gca,'Type','text');
set(txt,'FontSize', 11,'VerticalAlignment','middle','Rotation',89);
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]); 
grid on
if(EXPORT_TO_PDFS)
    print(graf3,'-dpdf','path_planning_time.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf3,'-dpng','path_planning_time.png');
end

graf4 = figure;
boxplot(path_plan_smoothness,'Labels',axis,'plotstyle','compact')
title 'Trajectory smoothness'
txt = findobj(gca,'Type','text');
set(txt,'FontSize', 11,'VerticalAlignment','middle','Rotation',89);
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]); 
grid on
if(EXPORT_TO_PDFS)
    print(graf4,'-dpdf','trajectory_smoothness.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf4,'-dpng','trajectory_smoothness.png');
end

graf5 = figure;
boxplot(path_simplify_length,'Labels',axis,'plotstyle','compact')
title 'Path simplify length'
txt = findobj(gca,'Type','text');
set(txt,'FontSize', 11,'VerticalAlignment','middle','Rotation',89);
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]);  
grid on
if(EXPORT_TO_PDFS)
    print(graf5,'-dpdf','path_simplify_length.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf5,'-dpng','path_simplify_length.png');
end

graf6 = figure;
boxplot(path_simplify_smoothness,'Labels',axis,'plotstyle','compact')
title 'Path simplify smoothness'
txt = findobj(gca,'Type','text');
set(txt,'FontSize', 11,'VerticalAlignment','middle','Rotation',89);
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]); 
grid on
if(EXPORT_TO_PDFS)
    print(graf6,'-dpdf','path_simplify_smoothness.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf6,'-dpng','path_simplify_smoothness.png');
end

axis = strrep(axis, ' ', '')
solved =(solved  / number_of_runs)*100;
graf2 = figure;
bar(solved)
title('Success rate')
set(gca,'Xticklabel',axis)
set(gcf, 'PaperPosition', [0 0 4 5]);
set(gcf, 'PaperSize', [4 5]); 
rotateticklabel(gca,60);
ylabel('%');
if(EXPORT_TO_PDFS)
    print(graf2,'-dpdf','success_rate.pdf');
end
if(EXPORT_TO_PNGS)
    print(graf2,'-dpng','success_rate.png');
end

mksqlite(db,'close');
