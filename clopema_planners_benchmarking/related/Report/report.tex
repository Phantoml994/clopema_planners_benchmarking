\documentclass[12pt,a4paper,oneside]{article}

\usepackage[rreport]{cmpcover}
%\usepackage[rreport,confidential]{cmpcover} % this is for non-public reports
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{hyperref}
\usepackage{caption}
\usepackage{subcaption}
\graphicspath{{fig/}}

 % THIS IS MANDATORY:
\title{OMPL path-planning algorithms' usage suitability study}
\author{Tom Jankovec}

%\CMPReportNo{CTU--CMP--1337--42}
%\CMPReportNo{CTU--CMP--0000--00\and K333--00/00}

\CMPAcknowledgement{\centering The research leading to these results has
received funding from the European Community's Seventh Framework Programme
(FP7/2007-2013) under grant agreement no. 288553}

 % THIS IS OPTIONAL BUT STRONGLY RECOMMENDED
\CMPEmail{jankotom@fel.cvut.cz}

%\CMPDocumentURL{}

 % THIS IS OPTIONAL
%\defRCSRevision{$Revision: 1.3.3.7 $}
%\CMPSubtitle{(Version \RCSRevision)}

%\CMPAdvisor{} % work advisor, if any

\newcommand{\graphwidth}{1}
\begin{document}
\maketitle
\begin{abstract}
The OMPL interface used in the CloPeMa project provides a variety of path-planning algorithms. The goal of this report is to determine which of these algorithms is the most suitable for our use.
\end{abstract}

\section{Introduction}
Despite the fact that the OMPL (Opem Motion Planning Library)\cite{OMPL} offers many available path-planning algorithms, some of these algorithms provide sub-optimal solutions for the CloPeMa project (a 13 degrees of freedom robot with two arms), or are unable to solve a great number of path-planning problems at all. Since the CloPeMa project uses an arbitrarily chosen implementation of the $RRTConnect$ algorithm, the goal of this report is to determine the suitability of this algorithm and whether a better path-planning algorithm could be used.\\  
This report utilizes the built-in MoveIT!\cite{MoveIT!} benchmarking interface, which offers an in-depth look at solutions provided by path-planning algorithms.

\section{Goal state selection}
The CloPeMa project uses goal poses as a way of denoting the desired robot arm's end effector (the gripper) position and orientation. A goal pose consists of three Cartesian coordinates and a quaternion. The selected goal poses were heavily biased toward situations which the robot may encounter: 20 goal poses directly above the table in four different gripper orientations (see fig. 1 and 2). Both experiments utilized this set of goal poses. Note that all goal poses were computed for one robot arm and the kinematic frame $tX\_ee$ which refers to the tip of the gripper.\\


\begin{figure}[H]
  \centering     
	\includegraphics[width=0.9\textwidth]{schema.png}\\
  \caption{Goal poses}
\end{figure}



\section{Path planning terms}
The benchmarking interface generates data for a variety of paths' properties. A description of some of the less self-explanatory properties is provided below.
\subsection*{Path clearance}
The formula used for computing clearance is:
$$ clearance = \frac{1}{n}\sum\limits_{i=0}^{n-1}cl(s_i) $$
where $n$ is the number of states along the path, $s_i$ is the i-th state along the path and $cl()$ is a function returning the distance to the nearest invalid state for a particular state. \cite{path_clearance}

\subsection*{Path smoothness}
The idea is to look at the triangles formed by consecutive path segments and compute the angle between those segments using Pythagora's theorem. Then, the outside angle for the computed angle is normalized by the path segments and contributes to the path smoothness. For a straight line path, the smoothness will be 0.
$$ smoothness = \sum\limits_{i=2}^{n-1}\left(\frac{2\left(\pi-arccos\left(\frac{a_i^2+b_i^2-c_i^2}{2a_ib_i}\right)\right)}{a_i+b_i}\right)^2$$
where $a_i=dist(s_{i-2},s{i-1},b_i=dist(s_{i-1},,s_i),c_i=dist(s_{i-2},s_i),s_i $ is the i-th state along the path and $dist(s_i,s_j)$ gives the distance between two states along the path. \cite{path_smoothness}

\subsection*{Path simplification}
The OMPL interface's description doesn't explicitly state which of the functions are utilised and neither does the benchmarking code. Most likely a function analogous to one or more of those described in the OMPL documentation\cite{path_simplification} is used. A common method of path simplification consists of picking two random points along the found path and trying to connect them with a straight line, thereby shortening and simplifying the said path.

\subsection*{Path interpolation}
Inserts a number of states in a path so that the path is made up of a specified number of states. States are inserted uniformly (more states on longer segments). Changes are performed only if a path has less than the specified number of states. \cite{path_interpolation}

\section{Running the benchmark interface}
The benchmark was run 25 times for each algorithm and the generated data were analysed using a script provided in the MoveIT! package and a user-created MATLAB script. Separate analysis was performed for each situation and timeout value.\\ The benchmark provides a series of $.log$ files containing text-based data, which the script then converts to an SQLite database. This database is then used to generate graphs with the MATLAB script. A description of the terms encountered in the generated database can be found at the end of this report.

\section{Results}
The values from the generated data were averaged over the 25 runs of each algorithm and over all goal poses. The resulting graphs are provided below.\\
Note that the data from unreachable goal poses were discarded. Also note that the lack of data for the $RRTStar$ algorithm in graphs representing properties of simplified paths in the first experiment is intentional, since the $RRTStar$ hasn't utilized any path-simplifying methods.
 
\subsection{The first experiment}
The focus of this experiment was the ability to quickly provide near-optimal solutions for commonly faced path-planning problems.
The robot's home position was used as the initial state and planning algorithms were run with a timeout of 1 second.\\
Note that the graph "Smoothness of the simplified path" consists of only zeroes since all paths were reduced to straight lines.
\begin{figure}[H]
  \centering     
	\includegraphics[width=0.9\textwidth]{goal_selection.png}
  \caption{Selected goal poses and the robot's initial position in the first experiment}
\end{figure}

\captionsetup[subfigure]{labelformat=empty}
\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/path_plan_length.pdf}
  \caption{Length of the initial found path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/success_rate.pdf}
  \caption{Rate of success}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/path_planning_time.pdf}
  \caption{Time spent finding unoptimised path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/trajectory_smoothness.pdf}
  \caption{Smoothness of the initial found path}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/path_simplify_length.pdf}
  \caption{Length of the simplified path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{easy/path_simplify_smoothness.pdf}
  \caption{Smoothness of the simplified path}
\end{subfigure}
\end{figure}

\subsection{The second experiment}
This experiment focused mainly on the algorithms' ability to provide any solutions at all to a set of difficult to solve path-planning problems, although other paths' properties were also examined. The robot's initial position was therefore located behind an obstacle (see fig. 3). The provided solutions were also computed for a single robot arm. The goal of this experiment was to eliminate algorithms which offer great results with simple configurations (i.e. the previous test) but fail to provide any solutions to more difficult problems. Separate timeout values of 1 and 5 seconds were used.
\begin{figure}[H]
  \centering     
	\includegraphics[width=0.9\textwidth]{difficult_initial_pose.png}\\
  \caption{Selected goals (identical to the first situation) and robot's initial position in the second experiment.}
\end{figure}
\subsubsection{1 second timeout}
\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/path_plan_length.pdf}
  \caption{Length of the initial found path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/success_rate.pdf}
  \caption{Rate of success}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/path_planning_time.pdf}
  \caption{Time spent finding unoptimised path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/trajectory_smoothness.pdf}
  \caption{Smoothness of the initial found path}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/path_simplify_length.pdf}
  \caption{Length of the simplified path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_1_second/path_simplify_smoothness.pdf}
  \caption{Smoothness of the simplified path}
\end{subfigure}
\end{figure}

\subsubsection{5 second timeout}
\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/path_plan_length.pdf}
  \caption{Length of the initial found path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/success_rate.pdf}
  \caption{Rate of success}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/path_planning_time.pdf}
  \caption{Time spent finding unoptimised path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/trajectory_smoothness.pdf}
  \caption{Smoothness of the initial found path}
\end{subfigure}
\end{figure}

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/path_simplify_length.pdf}
  \caption{Length of the simplified path}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=\graphwidth\linewidth]{difficult_5_second/path_simplify_smoothness.pdf}
  \caption{Smoothness of the simplified path}
\end{subfigure}
\end{figure}

\section{Conclusion}
The algorithms $LBMPiece$ an $BKPiece$ are obviously unsuitable for the CloPeMa project. Both offer by far the worst results, especially in terms of path length and rate of success. \\On the first glance the algorithms $PRM$ and $PRMStar$ appear to outperform all other algorithms in the majority of properties. However, this only applies for the simplest path-planning problems, since we can clearly see their low success rate in the second experiment. \\The already utilised $RRTConnect$ seems like the universally best performer in all scenarios, surpassed in only a few parameters by algorithms significantly worse in other, more important aspects. \\A future suggestion could be the comparison of several implementations of the  $RRTConnect$ algorithm with different parameters (which the OMPL allows) to find out if its performance can be increased to outperform the default configuration.
\newpage
\section{Benchmark data description}
This section provides a description of the variables used by the benchmarking interface.\\
\begin{description}
\item $experimentid$ \hfill \\ An index number assigned to a chunk of measured data before each database import.
\item $plannerid$ \hfill \\ An index number denoting the planner used for this computation. A list of goals with their respective indices can be found in the $known\_planner\_configs$ table.
\item $goal\_name$ \hfill \\ The name of the pose used as the goal state.
\item $path\_plan\_length$ \hfill \\ The length on the initial unoptimised found path.
\item $path\_simplify\_clearance$ \hfill \\ The clearance of the simplified path. 
\item $path\_simplify\_smoothness$ \hfill \\ The smoothness of the simplified path. The closer the value is to 0, the smoother the path is.
\item $path\_simplify\_time$ \hfill \\ The duration of the path simplification process.
\item $solved$ \hfill \\ A boolean value denoting whether a suitable path has been found (1-TRUE, 0-FALSE).
\item $process\_time$ \hfill \\ The duration of benchmark data generation.
\item $path\_simplify\_length$ \hfill \\ The length of the path after simplification.
\item $path\_interpolate\_time$ \hfill \\ The duration of path interpolation process.
\item $path\_plan\_smoothness$ \hfill \\ The smoothness of the initial unoptimised found path.
\item $path\_interpolate\_clearance$ \hfill \\ The clearance of the interpolated path.
\item $path\_plan\_time$ \hfill \\ The duration of planning of the initial unoptimised path.
\item $path\_interpolate\_smoothness$ \hfill \\ The smoothness of the interpolated path.
\item $path\_interpolate\_correct$ \hfill \\A boolean value denoting whether the interpolated path is collision-free.
\item $path\_interpolate\_length$ \hfill \\The length of the interpolated path.
\item $path\_plan\_clearance$ \hfill \\The clearance of the initial unoptimised found path.
\item $path\_plan\_correct$ \hfill \\A boolean value denoting whether the initial unoptimised found path is collision-free.
\end{description}

\begin{thebibliography}{9} %Reference
\bibitem{OMPL}
  \href{http://ompl.kavrakilab.org/}{OMPL homepage}
\bibitem{MoveIT!}
  \href{http://moveit.ros.org/}{MoveIT! homepage}
\bibitem{path_clearance}
  \href{http://ompl.kavrakilab.org/classompl_1_1geometric_1_1PathGeometric.html#acf32f1979b45d64904711f7b3661fd97}{ompl::geometric::PathGeometric::clearance() documentation}
\bibitem{path_smoothness}
  \href{http://ompl.kavrakilab.org/classompl_1_1geometric_1_1PathGeometric.html#acf32f1979b45d64904711f7b3661fd97}{ompl::geometric::PathGeometric::smoothness() documentation}
\bibitem{path_simplification}
  \href{http://ompl.kavrakilab.org/classompl_1_1geometric_1_1PathSimplifier.html#details}{ompl::geometric::PathSimplifier Class Reference}  
\bibitem{path_interpolation}
  \href{http://ompl.kavrakilab.org/classompl_1_1geometric_1_1PathGeometric.html#a29c50018bc71d4d25b0f8e4d3e97aecd}{ompl::geometric::PathGeometric::interpolate() documentation}
\end{thebibliography}

\end{document}
