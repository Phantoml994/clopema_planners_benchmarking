#include <clopema_planners_benchmarking/create_goal.h>

const std::string IP = "127.0.0.1"; //IP address of the warehouse server
const size_t PORT = 33829;          //Port of the warehouse server
const bool CLEAR_STORAGE = true;    //Clear the warehouse before attempting to add new objects

/*! \brief Converts degrees to radians
 *  \param deg The angle in degrees
 *  \return The angle in radians
 */
double degToRad(double deg) {
    return deg * (3.1415 / 180.0);
}

/*! \brief Creates a new goal state using Euler angles
 *  \param name Name of the goal state
 *  \param px,py,pz 3D Cartesian coordinates relative to the link specified in the add() function
 *  \param roll,pitch,yaw Euler angles of the goal state
 *  \return A new instance of the GoalState class
 */
GoalState createGoalStateEuler(std::string name, double px, double py, double pz, double roll, double pitch, double yaw) {
    tf::Quaternion quat = tf::createQuaternionFromRPY(roll, pitch, yaw);
    return createGoalState(name, px, py, pz, (double)quat.getX(), (double)quat.getY(), (double)quat.getZ(), (double)quat.getW());
}

/*! \brief Creates a new goal state
 *  \param name Name of the goal state
 *  \param px,py,pz 3D Cartesian coordinates relative to the link specified in the add() function
 *  \param ox,oy,oz,ow Quaternion parts (goal state rotation)
 *  \return A new instance of the GoalState class
 */
GoalState createGoalState(std::string name, double px, double py, double pz, double ox, double oy, double oz, double ow) {
    geometry_msgs::Pose pose;
    pose.position.x = px;
    pose.position.y = py;
    pose.position.z = pz;
    pose.orientation.x = ox;
    pose.orientation.y = oy;
    pose.orientation.z = oz;
    pose.orientation.w = ow;
    return GoalState(name, pose);
}

/*! \brief Establishes connection to the database, called before adding new goals.
 *  \warning Called only once.
 */
void dbConnect() {
    static bool called = false;
    if (called) {
        return;
    } else {
        std::vector<std::string> names;
        called = true;
        try {
            constraints_storage.reset ( new moveit_warehouse::ConstraintsStorage ( IP, PORT, 5.0 ) );
            robot_state_storage.reset (new moveit_warehouse::RobotStateStorage(IP, PORT, 5.0));
            ROS_INFO_STREAM ( "Successfully connected to databases " << constraints_storage->DATABASE_NAME << ", " << robot_state_storage->DATABASE_NAME);
        } catch ( std::runtime_error &ex ) {
            ROS_ERROR_STREAM ( "Connection failed: " << ex.what() );
            return;
        }


        ROS_INFO_STREAM("Existing robot states:");
        robot_state_storage->getKnownRobotStates(names);
        for (size_t i = 0; i < names.size(); i++) {
            ROS_INFO_STREAM(i << ": " << names[i]);
        }

        ROS_INFO_STREAM("Existing constraints:");
        constraints_storage->getKnownConstraints ( names );
        for ( std::size_t i = 0 ; i < names.size() ; i++ ) {
            ROS_INFO_STREAM (i << ": " << names[i] );
        }
    }
}

/*! \brief Adds goals to the database
 *  \param goals Vector of GoalState class instances
 */
void add (std::vector<GoalState> goals) {
    bool name_conflict;
    geometry_msgs::Pose goal_pose_relative, goal_pose;
    Eigen::Affine3d goal_offset, goal_pose_relative_eigen, goal_pose_eigen;
    std::vector<std::string>names;
    shape_msgs::SolidPrimitive sp;

    constraints_storage->getKnownConstraints ( names );

    clopema_robot::ClopemaRobotCommander g ( "arms" );
    robot_state::RobotState rs ( *g.getCurrentState() );

    goal_offset = rs.getGlobalLinkTransform ( "t3_desk" );

    sp.type = sp.SPHERE;
    sp.dimensions.resize ( 3, std::numeric_limits<float>::epsilon() * 10.0 );

    for (std::size_t i = 0; i < goals.size(); i++) {
        name_conflict = false;
        for (std::size_t j = 0; j < names.size(); j++) {
            if (goals[i].name == names[j]) {
                name_conflict = true;
                break;
            }
        }
        if (name_conflict) {
            ROS_ERROR_STREAM("Conflicting constraint names, constraint " << goals[i].name << " wasn't added");
            continue;
        }

        goal_pose_relative = goals[i].goal;
        tf::poseMsgToEigen ( goal_pose_relative, goal_pose_relative_eigen );
        goal_pose_eigen = goal_offset * goal_pose_relative_eigen;
        tf::poseEigenToMsg ( goal_pose_eigen, goal_pose );

        std::string buff;
        g.transform_to_tip(goal_pose, "r1_ee", goal_pose, buff);

        moveit_msgs::Constraints constraint;
        constraint.name = (boost::format("%s%d") % goals[i].name % i).str();

        moveit_msgs::PositionConstraint pc;
        pc.header.frame_id = "/base_link";
        pc.link_name = "r1_tip_link";
        pc.constraint_region.primitives.push_back ( sp );
        pc.constraint_region.primitive_poses.push_back ( goal_pose );
        pc.weight = 1.0;
        constraint.position_constraints.push_back ( pc );

        moveit_msgs::OrientationConstraint oc;
        oc.header.frame_id = pc.header.frame_id;
        oc.link_name = pc.link_name;
        oc.orientation = goals[i].goal.orientation;
        oc.absolute_x_axis_tolerance = oc.absolute_y_axis_tolerance = oc.absolute_z_axis_tolerance = std::numeric_limits<float>::epsilon() * 10.0;
        oc.weight = 1.0;
        constraint.orientation_constraints.push_back ( oc );

        try {
            constraints_storage->addConstraints ( constraint );
        } catch ( std::runtime_error &ex ) {
            ROS_ERROR_STREAM ( "Cannot save constraint " << goals[i].name << ": " << ex.what() );
        }
        ROS_INFO_STREAM ( "Added goal state " << goals[i].name << i );

    }
}

/*! \brief Adds goal states to the GoalStates vector in a distinctive manner
 *  \param &goals Vector for the states to be added in
 *  \param q Quaternion specifying goal state orientation
 */
void fillGoalsVector(std::vector<GoalState> & goals, Eigen::Quaterniond q) {
    goals.push_back(createGoalState("goal.",    0,    0, 0.1, q.x(), q.y(), q.z(), q.w()));
    goals.push_back(createGoalState("goal.",  0.2,  0.2, 0.1, q.x(), q.y(), q.z(), q.w()));
    goals.push_back(createGoalState("goal.",  0.2, -0.2, 0.1, q.x(), q.y(), q.z(), q.w()));
    goals.push_back(createGoalState("goal.", -0.2, -0.2, 0.1, q.x(), q.y(), q.z(), q.w()));
    goals.push_back(createGoalState("goal.", -0.2,  0.2, 0.1, q.x(), q.y(), q.z(), q.w()));;
}

/*!\brief Prints information about the specified goal state to the ROS_INFO_STREAM. Mainly for debugging purposes.
 * \param name Goal state name
 */
void printGoalStateInformation(std::string name) {
    moveit_warehouse::ConstraintsWithMetadata constr;
    bool exists = constraints_storage->getConstraints(constr, "name");
    if (!exists) {
        ROS_ERROR_STREAM("Goal state " << name << " doesn't exist!");
    } else {
        ROS_INFO_STREAM("Goal state " << name << ":\n" <<
                        "x:" << constr->position_constraints[0].constraint_region.primitive_poses[0].position.x << "," <<
                        "y:" << constr->position_constraints[0].constraint_region.primitive_poses[0].position.y << "," <<
                        "z:" << constr->position_constraints[0].constraint_region.primitive_poses[0].position.z << "\n" <<
                        "q.x:" << constr->orientation_constraints[0].orientation.x << "," <<
                        "q.y:" << constr->orientation_constraints[0].orientation.y << "," <<
                        "q.z:" << constr->orientation_constraints[0].orientation.z << "," <<
                        "q.w:" << constr->orientation_constraints[0].orientation.w);
    }
}

/*! \brief Removes ALL existing goal states and robot states
 */
void clearStorage() {
    std::vector<std::string> names;
    robot_state_storage->getKnownRobotStates(names);
    for (size_t i = 0; i < names.size(); i++) {
        ROS_INFO_STREAM("Removing robot state " << names[i]);
        robot_state_storage->removeRobotState(names[i]);
    }
    ROS_INFO_STREAM("Removed all robot states!");
    constraints_storage->getKnownConstraints(names);
    for (size_t i = 0; i < names.size(); i++) {
        ROS_INFO_STREAM("Removing goal state " << names[i]);
        constraints_storage->removeConstraints(names[i]);
    }
    ROS_INFO_STREAM("Removed all goal states!");
}
/*! \brief Adds initial robot states to the database
 */
void addInitialRobotStates() {
    ros::AsyncSpinner spinner(1);
    spinner.start();
    ROS_INFO_STREAM("Adding initial benchmarking states.");
    clopema_robot::ClopemaRobotCommander g1 ( "arms" );
    clopema_robot::ClopemaRobotCommander g2 ( "ext" );
    clopema_robot::ClopemaRobotCommander g3 ( "r1_arm" );
    g1.move_to_named_target("home_arms");
    ros::Duration(0.5).sleep();
    g2.move_to_named_target("ext_minus_90");
    ros::Duration(0.5).sleep();
    robot_state::RobotState rs ( *g2.getCurrentState() );
    moveit_msgs::RobotState msg;
    robot_state::robotStateToRobotStateMsg(rs, msg);

    try {
        robot_state_storage->addRobotState(msg, "clopema_rotated_initial_pose");
        ROS_INFO_STREAM("Added the initial benchmarking state.");
    } catch (std::runtime_error &ex) {
        ROS_ERROR("Cannot save robot state to the database: %s", ex.what());
    }

    bool poseReachable, poseReached;
    geometry_msgs::Pose pose;
    g3.setStartStateToCurrentState();
    pose.position.x = -0.6;
    pose.position.y = 0;
    pose.position.z = 0.8;
    pose.orientation.y = -1;
    poseReachable =  g3.setJointValueTarget(pose, "r1_tip_link");
    poseReached = g3.move();
    ROS_INFO_STREAM("Pose reachable: " << poseReachable << ", pose reached: " << poseReached);
    ros::Duration(0.5).sleep();
    if (!poseReached) {
        ROS_ERROR_STREAM("Couldn't reach the difficult pose, try increasing your path-planning algorithm's timeout");
    }
    else {
        robot_state::RobotState rs2 ( *g3.getCurrentState() );
        robot_state::robotStateToRobotStateMsg(rs2, msg);
        try
        {
            robot_state_storage->addRobotState(msg, "clopema_rotated_difficult_initial_pose");
            ROS_INFO_STREAM("Added the difficult initial benchmarking state.");
        }
        catch (std::runtime_error &ex)
        {
            ROS_ERROR_STREAM("Cannot save robot state to the database: " << ex.what());
        }
    }
    spinner.stop();
}

int main ( int argc, char **argv ) {
    ros::init ( argc, argv, "create_goal" );

    std::vector<GoalState> goals;

    using namespace Eigen;
    Quaterniond q;

    q = Quaterniond(AngleAxisd(M_PI * (4.0 / 6.0), Vector3d::UnitY()) * Quaterniond(AngleAxisd(M_PI, Vector3d::UnitZ())));
    fillGoalsVector(goals, q);
    q = Quaterniond(AngleAxisd(M_PI / 2, Vector3d::UnitY())) * Quaterniond(AngleAxisd(M_PI, Vector3d::UnitX())) * Quaterniond(AngleAxisd(M_PI / 6, Vector3d::UnitY()) * Quaterniond(AngleAxisd(M_PI, Vector3d::UnitZ())));
    fillGoalsVector(goals, q);
    q = Quaterniond(AngleAxisd(M_PI / 2, Vector3d::UnitY())) * Quaterniond(AngleAxisd(M_PI / 2, Vector3d::UnitX())) * Quaterniond(AngleAxisd(M_PI / 6, Vector3d::UnitY())) * Quaterniond(AngleAxisd(M_PI , Vector3d::UnitZ()));
    fillGoalsVector(goals, q);
    q = Quaterniond(AngleAxisd(M_PI / 2, Vector3d::UnitY())) * Quaterniond(AngleAxisd(-M_PI / 2, Vector3d::UnitX())) * Quaterniond(AngleAxisd(M_PI / 6, Vector3d::UnitY())) * Quaterniond(AngleAxisd(M_PI , Vector3d::UnitZ()));
    fillGoalsVector(goals, q);

    dbConnect();
    if (CLEAR_STORAGE) {
        clearStorage();
    }
    addInitialRobotStates();
    add(goals);

    return 0;
}