#ifndef _CREATE_GOAL_HEADER
#define _CREATE_GOAL_HEADER

#include <string>
#include <stdio.h>
#include <ros/ros.h>
#include <eigen_conversions/eigen_msg.h>
#include <clopema_robot/robot_commander.h>
#include <boost/icl/type_traits/to_string.hpp>
#include <moveit/robot_state/conversions.h>
#include <moveit/warehouse/state_storage.h>
#include <boost/math/constants/constants.hpp>
#include <moveit/kinematic_constraints/utils.h>
#include <moveit/warehouse/constraints_storage.h>
#include <moveit/planning_interface/planning_interface.h>
#include <boost/format.hpp>

//Warehouse
boost::shared_ptr<moveit_warehouse::ConstraintsStorage> constraints_storage;
boost::shared_ptr<moveit_warehouse::RobotStateStorage> robot_state_storage;

class GoalState {
public:
    std::string name;
    geometry_msgs::Pose goal;

    GoalState() {
        name = "invalid_state";
        goal = geometry_msgs::Pose();
    }
    GoalState(std::string name, geometry_msgs::Pose goal) {
        this->name = name;
        this->goal = goal;
    }
};

void add (std::vector<GoalState> goals);
void addInitialRobotStates();
void clearStorage();
GoalState createGoalState(std::string name, double px, double py, double pz, double ox, double oy, double oz, double ow);
GoalState createGoalStateEuler(std::string name, double px, double py, double pz, double roll, double pitch, double yaw);
void dbConnect();
double degToRad(double deg);
void fillGoalsVector(std::vector<GoalState> & goals, Eigen::Quaterniond q) ;
void printGoalStateInformation(std::string name);

#endif