##############################
#  HOW TO SET EVERYTHING UP  #
############################## 

(Additional information regarding MoveIT benchmarks is available at http://moveit.ros.org/wiki/Benchmarking )


1). Compile everything

2). Start the warehouse using "roslaunch clopema_launch virtual_robot.launch", connect  rViz to the warehouse (check MoveIT checkbox, press Connect)

3). (Optional) Start the GUI using "rosrun moveit_ros_benchmarks_gui moveit_benchmark_gui", connect GUI to the warehouse

4). Create a scene "benchmark" in rViz: Motion Planning -> Stored Scenes -> Save scene -> Double click on the scene name, rename

5). Create the initial and goal states using "rosrun clopema_planners_benchmarking create_goal"

6). (Optional) Check everything in the GUI after refreshing (the button to the left of the plus button)

7). Set a proper output location in the file clopema_planners_benchmarking/config.cfg (edit other parameters to your liking, the 'timeout' variable denotes how many seconds each algorithm gets to run, the 'runs' variable in the [plugin] section denotes the number of times each algorithm is run)
### WARNING ### The 'path' variable must point to an existing folder which doesn't contain any files with conflicting names (benchmark.*.log), otherwise the data is discarded without any warning!

8). Run the benchmarks using "roslaunch clopema_planners_benchmarking run_benchmark_ompl.launch"

9). Add all the benchmark.*.log files to a database using "rosrun moveit_ros_benchmarks moveit_benchmark_statistics.py /LOCATION_OF_RESULTS/benchmark.\N.log" where \N is the index of the .log file. (You can use regex: 'benchmark.*', but the .log files won't be added in the right order)

10). Locate the benchmark.db file (should be in the folder from which you ran the .log files processing command) and move it to the clopema_planners_benchmarking/related directory. 

11). Edit the variables defined in the first several lines of benchmark_visualizer.m (in the 'related' directory) to your liking, then run the file in MATLAB. IDS_TO_USE is an array of integers, each integer denoting a data from a single log file in their order of adding (if IDS_TO_USE = [2,3], only the data from the second and third added .log file will be used).

12). Now these points of data make a beautiful line

